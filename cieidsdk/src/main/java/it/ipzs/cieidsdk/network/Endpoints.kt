package it.ipzs.cieidsdk.network

object Endpoints {

    const val idp = "Authn/SSL/Login2"
    const val idpX509AuthIbrido = "Authn/SSL/X509AuthIbrido"

}