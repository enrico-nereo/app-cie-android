# app CIE

## WARNING

MOVED TO https://github.com/sagento/app-cie-android



### Disclaimer

Quest'app non ha ancora raggiunto uno stadio di 'completezza' tale da poter essere distribuita, il suo utilizzo è sconsigliato.

### Credits

App basata su [questo sdk](https://github.com/italia/cieid-android-sdk)

Librerie open source usate:

* https://github.com/square/okhttp
* https://github.com/square/retrofit
* https://github.com/zxing/zxing

